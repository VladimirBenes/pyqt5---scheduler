import NewWindow
import HelpClasses
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.QtCore import pyqtSlot, QDate
from PyQt5 import uic, QtCore
import GlobalVariables

main_dialog = uic.loadUiType("Design/GUI.ui")[0]


class MyApp(QMainWindow, main_dialog):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setupUi(self)
        self.loadWidged()
        self.data = HelpClasses.loadData()
        self.data.saveDataToList()
        self.fillComboWithData()

    def fillComboWithData(self):
        self.data.fillComboBox(self.cbPrio, GlobalVariables.listGlobalPrio)
        self.data.fillComboBox(self.cbGrp, GlobalVariables.listGlobalGrp)

    def loadWidged(self):
        self.cbPrio = self.comboBox
        self.cbGrp = self.comboBox_2
        self.lwt = self.listWidget
        self.calendar = self.calendarWidget

    def runAddWin(self):
        add = NewWindow.AddWindow(self)
        add.exec()
        self.fillComboWithData()

    @pyqtSlot(bool)
    def on_pushButton_clicked(self):
        self.runAddWin()

    @pyqtSlot(QDate)
    def on_calendarWidget_activated(self):
        date = self.sender().selectedDate()
        self.runAddWin()

    @pyqtSlot(str)
    def on_comboBox_activated(self):
        print(self.sender().currentText())

    def closeEvent(self, event):
        self.data.writeToFile("Data/grp.txt", GlobalVariables.listGlobalGrp)
        self.data.writeToFile("Data/prio.txt", GlobalVariables.listGlobalPrio)

    def run(self):
        self.show()
        self.app.exec()

def main(self):
    app = QApplication([])
    window = MyApp()
    window.app = app
    window.run()


if __name__ == "__main__":
    main()
