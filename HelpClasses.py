import os.path

from PyQt5.QtWidgets import QListWidgetItem

import GlobalVariables as gv
import customWid


class loadData:

    def __init__(self):
        pass

    def openFile(self, filename):
        filename = filename
        ls = []
        if os.path.exists(filename):
            f = open(filename, "r+")
            for x in f:
                ls.append(x.strip())
            f.close()
        return ls

    def writeToFile(self, filename, ls):
        filename = filename
        ls = ls
        if os.path.exists(filename):
            f = open(filename, "w+")
            for x in ls:
                f.write(x + "\n")
        else:
            self.openFile(filename)

    def saveDataToList(self):
        global listGlobalPrio
        global listGlobalGrp
        gv.listGlobalGrp = self.openFile("Data/grp.txt")
        gv.listGlobalPrio = self.openFile("Data/prio.txt")

    def fillComboBox(self, comboBoxName, ls):
        comboBoxName.clear()
        comboBoxName.addItems(ls)

    def fillListWidget(self, widgetName, ls):
        for x in ls:
            self.addCustomWidget(x, widgetName)

    def fillMainListWidget(self, widgetName, ls):
        for x in ls:
            self.addCustomWidgetTask(x["name"], x["date"], x["description"], x["priority"], x["group"], widgetName)

    def addCustomWidget(self, text, obj1):
        obj1 = obj1
        cw = customWid.CustomWidged()
        item = QListWidgetItem(obj1)
        item.setSizeHint(cw.minimumSizeHint())
        obj1.addItem(item)
        obj1.setItemWidget(item, cw)
        cw.get_object_value(text)

    def addCustomWidgetTask(self, name, date, description, priority, group, obj1):
        obj1 = obj1
        cw = customWid.TaskWidget()
        item = QListWidgetItem(obj1)
        item.setSizeHint(cw.minimumSizeHint())
        obj1.addItem(item)
        obj1.setItemWidget(item, cw)
        cw.get_object_value(name, date, description, priority, group)

