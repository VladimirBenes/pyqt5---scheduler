from PyQt5.QtWidgets import QWidget, QColorDialog
from PyQt5.QtCore import pyqtSlot
from PyQt5 import uic
import HelpClasses
import GlobalVariables as gv

custom_widged = uic.loadUiType("Design/customWidged.ui")[0]
task_widged = uic.loadUiType("Design/taskWidget.ui")[0]


class CustomWidged(QWidget, custom_widged):

    def __init__(self):
        QWidget.__init__(self)
        self.setupUi(self)
        self.l = self.label

    @pyqtSlot(str)
    def get_object_value(self, val):
        self.l.setText(val)

    @pyqtSlot()
    def on_pushButton_2_clicked(self):
        color = QColorDialog.getColor()
        self.sender().setStyleSheet("QWidget { background-color: %s}" % color.name())

    @pyqtSlot()
    def on_pushButton_clicked(self):
        textToRemove = self.l.text()
        lw = self.parent().parent()
        aw = self.window()
        cbPrio = aw.cbPrio
        cbGrp = aw.cbGrp
        if textToRemove in gv.listGlobalPrio:
            self.deleteItemFromLists(gv.listGlobalPrio, textToRemove, lw, cbPrio)
        else:
            self.deleteItemFromLists(gv.listGlobalGrp, textToRemove, lw, cbGrp)

    def deleteItemFromLists(self, ls, textToRemove, lw, cb):
        hclass = HelpClasses.loadData
        ls.remove(textToRemove)
        lw.clear()
        hclass.fillListWidget(hclass(), lw, ls)
        cb.clear()
        hclass.fillComboBox(hclass,cb, ls)


class TaskWidget(QWidget, task_widged):
    def __init__(self):
        QWidget.__init__(self)
        self.setupUi(self)
        self.label_1 = self.label
        self.labael_2 = self.label_2
        self.label_3 = self.label_3
        self.label_4 = self.label_4
        self.label_5 = self.label_5
        gv.listOfWid.append(self)
        self.curIndex = gv.listOfWid.index(self)

    @pyqtSlot(str)
    def get_object_value(self, val, val2, val3, val4, val5):
        self.label_1.setText(val)
        self.label_2.setText(val3)
        self.label_3.setText(val2)
        self.label_4.setText(val4)
        self.label_5.setText(val5)

    @pyqtSlot()
    def on_pushButton_clicked(self):
        cw = self.sender().parent()
        lw = cw.parent().parent()

        self.deleteItemFromLists(gv.listGlobalData, self.curIndex, lw)

    def deleteItemFromLists(self, ls, indexToRemove, lw):
        hclass = HelpClasses.loadData
        del ls[indexToRemove]
        gv.listOfWid.remove(self)
        lw.clear()
        gv.listOfWid.clear()
        hclass.fillMainListWidget(hclass(), lw, ls)
