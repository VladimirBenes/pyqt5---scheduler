import datetime
from PyQt5.QtWidgets import QDialog, QColorDialog
from PyQt5.QtCore import pyqtSlot
from PyQt5 import uic
import GlobalVariables as gv
import HelpClasses
import customWid

add_dialog = uic.loadUiType("Design/addWindow.ui")[0]


class AddWindow(QDialog, add_dialog):
    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self.now = datetime.datetime.now()
        self.setupUi(self)
        self.loadWidged()
        self.fillWidgedWithData()
        #self.de.setDate(datetime.datetime.now())
        self.tw.setCurrentIndex(0)
        self.de.setDate(self.Qdate)

    def fillWidgedWithData(self):
        self.data = HelpClasses.loadData()
        self.data.fillComboBox(self.cbPrio, gv.listGlobalPrio)
        self.data.fillComboBox(self.cbGrp, gv.listGlobalGrp)
        self.data.fillListWidget(self.lwPrio, gv.listGlobalPrio)
        self.data.fillListWidget(self.lwGrp, gv.listGlobalGrp)

    def loadWidged(self):
        self.cbPrio = self.comboBox
        self.cbGrp = self.comboBox_2
        self.tbPrio = self.lineEdit_2
        self.tbGrp = self.lineEdit_3
        self.lwPrio = self.listWidget
        self.lwGrp = self.listWidget_2
        self.lePrio = self.lineEdit_4
        self.leGrp = self.lineEdit_5
        self.tw = self.tabWidget
        self.de = self.dateEdit
        self.le = self.lineEdit
        self.te = self.textEdit
        self.de = self.dateEdit
        self.Qdate = self.window().parent().calendar.selectedDate()

    def on_comboBox_activated(self):
        self.cbDataPrio = self.sender().currentText()

    def on_comboBox_2_activated(self):
        self.cbDataPrio = self.sender().currentText()

    @pyqtSlot()
    def on_pushButton_clicked(self):
        name = self.le.text()
        description = self.te.toPlainText()
        date = self.de.date()
        date = date.toString('MMMM dd, yyyy')

        if gv.listGlobalPrio and gv.listGlobalGrp:
            self.cbDataPrio = gv.listGlobalPrio[0]
            self.cbDataGrp = gv.listGlobalGrp[0]
        else:
            self.cbDataPrio = None
            self.cbDataGrp = None

        priority = self.cbDataPrio
        group = self.cbDataGrp

        baseData = {"name": name, "date": date, "description" : description, "priority" : self.cbDataPrio, "group" : self.cbDataGrp}
        gv.listGlobalData.append(baseData)
        #s = self.sender().parent().parent().parent().parent().parent().parent()
        s = self.window().parent()
        self.data.addCustomWidgetTask(name, date, description, priority, group, s.lwt)
        self.close()


    @pyqtSlot(int)
    def on_checkBox_stateChanged(self, i):
        self.disable(self.tbPrio, i)

    @pyqtSlot(int)
    def on_checkBox_2_stateChanged(self, i):
        self.disable(self.tbGrp, i)

    @pyqtSlot(bool)
    def on_pushButton_2_clicked(self):
        self.addIt(self.lePrio, self.lwPrio, self.cbPrio, gv.listGlobalPrio)


    @pyqtSlot(bool)
    def on_pushButton_3_clicked(self):
        self.addIt(self.leGrp, self.lwGrp, self.cbGrp, gv.listGlobalGrp)

    def addIt(self, obj, obj1, obj2, ls):
        obj = obj
        obj1 = obj1
        obj2 = obj2
        ls = ls
        text = obj.text()
        if text:
            self.data.addCustomWidget(text, obj1)
            ls.append(obj.text())
            self.data.fillComboBox(obj2, ls)
            obj.setText("")
            obj.setFocus(True)

    def disable(self, obj, i):
        obj = obj
        if i == 0:
            obj.setDisabled(True)
        elif i == 2:
            obj.setDisabled(False)
